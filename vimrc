" Hit <esc> quicker (best vim shortcut ever)
inoremap jk <esc>
snoremap jk <esc>

" Navigate through wrapped lines
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" Disable all bells
set belloff=all
